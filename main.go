package main

import (
	"fmt"
	"log"

	"github.com/google/go-jsonnet"
)

func main() {
	vm := jsonnet.MakeVM()
	snippet := `{
    person1: {
      name: "Jekyll",
      welcome: "Hello " + self.name + "!",
    },
    person2: self.person1 { name: "Hyde" }, 
    person3: self.person1 { name: "Ted" }, 
    person4: self.person1 { name: "Alice" }, 
  }`
	jsonStr, err := vm.EvaluateAnonymousSnippet("example1.jsonnet", snippet)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(jsonStr)
}

/*
output:
{
   "person1": {
      "name": "Jekyll",
      "welcome": "Hello Jekyll!"
   },
   "person2": {
      "name": "Hyde"
      "welcome": "Hello Hyde!"
   },
   "person3": {
      "name": "Ted"
      "welcome": "Hello Ted!"
   },
   "person4": {
      "name": "Alice"
      "welcome": "Hello Alice!"
   }
}
*/
